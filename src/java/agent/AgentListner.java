package agent;

import jason.asSyntax.Literal;

import java.util.List;

public interface AgentListner {

	public void notifyPercepts(List<Literal> percepts);
	
}
