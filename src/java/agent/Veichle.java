package agent;

import jason.JasonException;
import jason.asSemantics.Agent;
import jason.asSemantics.Event;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import jason.asSyntax.Literal;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBody.BodyType;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;

public class Veichle extends Agent {

	private boolean act;

	public Veichle() {
		act = false;
	}

	@Override
	public void buf(List<Literal> percepts) {
		if (percepts == MarsAgentArch.PREVIOUS_PERCEPTIONS)
			return;
		super.buf(percepts);
		act = true;
	}

	private List<Option> getOptions(Event event) {
		List<Option> options = null;
		try {
			options = this.ts.relevantPlans(event.getTrigger());
			options = this.ts.applicablePlans(options);
		} catch (JasonException e) {
			e.printStackTrace();
		}
		return options;
	}

	private boolean hasPlansWithoutActions(List<Option> optionsList) {
		if (optionsList == null)
			return false;
		for (Option option : optionsList) {
			if (!isAction(option.getPlan().getBody()))
				return true;
		}
		return false;
	}

	private boolean isAction(PlanBody formulae) {
		return formulae.getBodyType() == BodyType.action;
	}

	private Intention pollIntention(Queue<Intention> intentions,
			boolean withAction) {
		Iterator<Intention> intentionsIterator = intentions.iterator();
		while (intentionsIterator.hasNext()) {
			Intention intention = intentionsIterator.next();
			IntendedMeans im = intention.peek();
			if (im == null)
				continue;
			PlanBody formulae = im.getCurrentStep();
			if (formulae == null)
				continue;
			if (isAction(formulae) == withAction) {
				intentionsIterator.remove();
				return intention;
			}
		}
		return null;
	}

	@Override
	public Event selectEvent(Queue<Event> events) {
		Iterator<Event> eventsIterator = events.iterator();
		while (eventsIterator.hasNext()) {
			Event event = eventsIterator.next();
			List<Option> options = getOptions(event);
			if (act || hasPlansWithoutActions(options)) {
				eventsIterator.remove();
				return event;
			}
		}
		return null;
	}

	@Override
	public Intention selectIntention(Queue<Intention> intentions) {
		Intention intention = null;
		if (act)
			intention = pollIntention(intentions, true);
		if (intention == null)
			intention = pollIntention(intentions, false);
		else
			act = false;
		return intention;
	}

	@Override
	public Option selectOption(List<Option> options) {
		Iterator<Option> optionsIterator = options.iterator();
		while (optionsIterator.hasNext()) {
			Option option = optionsIterator.next();
			if (act || !isAction(option.getPlan().getBody())) {
				optionsIterator.remove();
				return option;
			}
		}
		return null;
	}

}
