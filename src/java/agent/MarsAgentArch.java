package agent;

import jason.architecture.AgArch;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTermImpl;

import java.util.List;

import mapModel.MapModel;

public class MarsAgentArch extends AgArch {

	private static final String ENABLED_VEICHLE = "normal";
	private static final String[] MAP_PERCEPTS = { "vertices", "visibleVertex",
			"visibleEdge", "probedVertex", "surveyedEdge", "position",
			"visibleEntity" };
	protected static final String MAP_PERCEPTS_MSG_ID = "1";
	protected static MapModel mapModel;
	private static final String MY_TEAM = "a";
	public static final List<Literal> PREVIOUS_PERCEPTIONS = null;
	private static final int TEAMS = 2;
	protected static final int VEICHLES = 28;
	public static MapModel getMapModel() {
		return mapModel;
	}

	public static boolean hasAllTeamPositions() {
		if(mapModel == null)
			return false;
		return mapModel.hasAllTeamPositions();
	}
	
	protected String username;

	public MarsAgentArch() {
		super();
	}

	protected Literal cleanedPercept(Literal percept) {
		Literal copy = percept.copy();
		copy.delSources();
		return copy;
	}

	private static void createMapModel(List<Literal> percepts) {
		Literal verticesPer = findPercept("vertices", percepts);
		int vertices = (int) numberTermOf(0, verticesPer);
		mapModel = new MapModel(vertices, TEAMS, VEICHLES, MY_TEAM);
	}

	protected static Literal findPercept(String type, List<Literal> percepts) {
		for (Literal percept : percepts)
			if (hasType(percept, type))
				return percept;
		return null;
	}

	protected static boolean hasType(Literal percept, String type) {
		return percept.getFunctor().equals(type);
	}

	protected boolean isDynamicMapPercept(Literal percept) {
		return hasType(percept, "position")
				|| hasType(percept, "vertices");
	}

	protected boolean isMapPercept(Literal percept) {
		for (String type : MAP_PERCEPTS)
			if (hasType(percept, type))
				return true;
		return false;
	}

	private static double numberTermOf(int i, Literal literal) {
		return ((NumberTermImpl) literal.getTerm(i)).solve();
	}

	private static String stringTermOf(int i, Literal percept) {
		Atom atom = (Atom) percept.getTerm(i);
		String str = atom.getFunctor();
		return str;
	}

	protected synchronized static void updateMapModel(List<Literal> percepts) {
		if (mapModel == null)
			createMapModel(percepts);
		for (Literal percept : percepts) {
			if (hasType(percept, "visibleVertex")) {
				String vertex = stringTermOf(0, percept);
				String team = stringTermOf(1, percept);
				if (!team.equals("none"))
					mapModel.setVertex(vertex, team);
			} else if (hasType(percept, "visibleEdge")) {
				String vertex1 = stringTermOf(0, percept);
				String vertex2 = stringTermOf(1, percept);
				mapModel.addEdge(vertex1, vertex2);
			} else if (hasType(percept, "probedVertex")) {
				String vertex = stringTermOf(0, percept);
				double value = numberTermOf(1, percept);
				mapModel.setVertex(vertex, value);
			} else if (hasType(percept, "surveyedEdge")) {
				String vertex1 = stringTermOf(0, percept);
				String vertex2 = stringTermOf(1, percept);
				double cost = numberTermOf(2, percept);
				mapModel.addEdge(vertex1, vertex2, cost);
			} else if (hasType(percept, "visibleEntity")) {
				String veichle = stringTermOf(0, percept);
				String vertex = stringTermOf(1, percept);
				String team = stringTermOf(2, percept);
				boolean enabled = stringTermOf(3, percept).equals(
						ENABLED_VEICHLE);
				mapModel.putVeichle(veichle, vertex, team, enabled);
			}
		}
	}

}