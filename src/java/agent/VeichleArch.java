package agent;

import jason.asSyntax.Literal;

import java.util.List;

import env.EismassimEnv;

public class VeichleArch extends MarsAgentArch implements AgentListner {

	private static int perceptsNotifications = 0;
	private static final int SLEEP_TIME = 500;
	
	private synchronized static void resetMapModel() {
		if (perceptsNotifications == VEICHLES) {
			mapModel.reset();
			perceptsNotifications = 0;
		}
		perceptsNotifications++;
	}
	
	private boolean listnerAttached;

	private List<Literal> percepts;

	private List<Literal> getPercepts() {
		List<Literal> percepts = this.percepts;
		this.percepts = PREVIOUS_PERCEPTIONS;
		return percepts;
	}

	private void initListner() {
		if (!listnerAttached || username==null) {
			this.username = EismassimEnv.attachMarsAgentListner(
					this.getAgName(), this);
	        getTS().getAg().getBB().add(Literal.parseLiteral("username(" + username + ")"));
	        getTS().getAg().getBB().add(Literal.parseLiteral("agentname(" + this.getAgName()+")"));
			listnerAttached = true;
		}
	}

	public void notifyPercepts(List<Literal> percepts) {
		resetMapModel();
		this.percepts = percepts;
	}

	@Override
	public List<Literal> perceive() {
		initListner();
		List<Literal> percepts = getPercepts();
		if (percepts == PREVIOUS_PERCEPTIONS) {
			sleep(SLEEP_TIME);
			return PREVIOUS_PERCEPTIONS;
		}
		updateMapModel(percepts);
		return percepts;
	}

	private void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
