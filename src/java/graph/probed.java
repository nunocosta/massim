// Internal action code for project massim

package graph;

import mapModel.MapModel;
import agent.MarsAgentArch;
import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class probed extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	String pos = ((Atom)args[0]).getFunctor();
        MapModel mapModel = MarsAgentArch.getMapModel();
        return mapModel.isVertexProbed(pos);
    }
}
