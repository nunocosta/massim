// Internal action code for project massim

package graph;

import agent.MarsAgentArch;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class knownTeamPositions extends DefaultInternalAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        return MarsAgentArch.hasAllTeamPositions();
    }
}
