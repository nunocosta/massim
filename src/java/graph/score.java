// Internal action code for project massim

package graph;

import agent.MarsAgentArch;
import mapModel.MapModel;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class score extends DefaultInternalAction {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
      MapModel mapModel =  MarsAgentArch.getMapModel();
      Term score = new NumberTermImpl(mapModel.getScore());
      return un.unifies(score, args[0]);
    }
}
