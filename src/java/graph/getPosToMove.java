// Internal action code for project massim

package graph;

import mapModel.MapModel;
import agent.MarsAgentArch;
import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class getPosToMove extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	MapModel mapModel = MarsAgentArch.getMapModel();
    	String pos = ((Atom)args[0]).getFunctor();
    	String role = ((Atom)args[1]).getFunctor();
    	String nextPos = mapModel.getPosToMove(pos,role);
    	
        return un.unifies(new Atom(nextPos), args[2]);
    }
}
