// Internal action code for project massim

package graph;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.util.List;

import mapModel.MapModel;
import agent.MarsAgentArch;


public class direction extends DefaultInternalAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4144956211455151872L;
	
	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args)
			throws Exception {		
		MapModel mapModel = MarsAgentArch.getMapModel();
		String source = ((Atom) args[0]).getFunctor();
		String goal = ((Atom) args[1]).getFunctor();
		double maxEnergy = ((NumberTermImpl) args[2]).solve();
		List<String> path = mapModel.getPath(source, goal, 1, 1/(0.5*maxEnergy));
		if(path == null)
			return false;
		return un.unifies(new Atom(path.get(1)), args[3]);
	}
}
