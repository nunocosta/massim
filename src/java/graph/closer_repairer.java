// Internal action code for project massim

package graph;

import mapModel.MapModel;
import agent.MarsAgentArch;
import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class closer_repairer extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	MapModel mapModel = MarsAgentArch.getMapModel();
		String my_pos = ((Atom) args[0]).getFunctor();
		String r1_pos = ((Atom) args[2]).getFunctor();
		String r2_pos = ((Atom) args[4]).getFunctor();
		int maxEnergy = 8;
		double distance1 = mapModel.getDistance(my_pos, r1_pos, 1, 1/(0.5*maxEnergy));
		double distance2 = mapModel.getDistance(my_pos, r2_pos, 1, 1/(0.5*maxEnergy));
		if (distance1 == Integer.MAX_VALUE & distance2 == Integer.MAX_VALUE)
			return false;
		if (distance1 < distance2)
			return un.unifies(args[1], args[5]);
		else
			return un.unifies(args[3], args[5]);
    }
}
