// Internal action code for project massim

package graph;

import mapModel.MapModel;
import agent.MarsAgentArch;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

public class distance extends DefaultInternalAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args)
			throws Exception {
		if(args[0].equals(args[1]))
			return un.unifies(new NumberTermImpl(0), args[2]);
		MapModel mapModel = MarsAgentArch.getMapModel();
		String source = ((Atom) args[0]).getFunctor();
		String goal = ((Atom) args[1]).getFunctor();
		double distance = mapModel.getDistance(source, goal, 1, 0);
		if(distance==Integer.MAX_VALUE)
			return false;
		return un.unifies(new NumberTermImpl(distance), args[2]);
	}
}
