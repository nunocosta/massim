// Internal action code for project massim

package graph;

import mapModel.MapModel;
import agent.MarsAgentArch;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

public class closer extends DefaultInternalAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5580560961184130063L;

	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args)
			throws Exception {
		MapModel mapModel = MarsAgentArch.getMapModel();
		String start = ((Atom) args[0]).getFunctor();
		String goal1 = ((Atom) args[1]).getFunctor();
		String goal2 = ((Atom) args[2]).getFunctor();
		int maxEnergy = (int) ((NumberTermImpl)args[3]).solve();
		double distance1 = mapModel.getDistance(start, goal1, 1, 1/(0.5*maxEnergy));
		double distance2 = mapModel.getDistance(start, goal2, 1, 1/(0.5*maxEnergy));
		if (distance1 < distance2)
			return un.unifies(args[1], args[4]);
		else
			return un.unifies(args[2], args[4]);
	}
}
