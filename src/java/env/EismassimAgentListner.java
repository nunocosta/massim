package env;

import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import agent.AgentListner;

import eis.AgentListener;
import eis.EnvironmentInterfaceStandard;
import eis.exceptions.ActException;
import eis.exceptions.AgentException;
import eis.exceptions.RelationException;
import eis.iilang.Action;
import eis.iilang.Identifier;
import eis.iilang.Parameter;
import eis.iilang.Percept;

public class EismassimAgentListner implements AgentListener {

	private EnvironmentInterfaceStandard eisEnv;
	private int handledPers;
	private Map<String, AgentListner> marsAgentListners;
	private String name;
	private int persOfStep;

	public EismassimAgentListner(EnvironmentInterfaceStandard eisEnv,
			String name, String entity,
			Map<String, AgentListner> marsAgentListner)
					throws AgentException, RelationException {
		eisEnv.registerAgent(name);
		eisEnv.associateEntity(name, entity);
		eisEnv.attachAgentListener(name, this);
		this.eisEnv = eisEnv;
		this.name = name;
		handledPers = 0;
		persOfStep = 0;
		this.marsAgentListners = marsAgentListner;
	}

	private Action actionOf(Structure sAction) {
		LinkedList<Parameter> parameters = new LinkedList<Parameter>();
		for (Term term : sAction.getTerms())
			parameters.add(new Identifier(term.toString()));
		return new Action(sAction.getFunctor(), parameters);
	}

	public boolean executeAction(Structure action) {
		try {
			eisEnv.performAction(name, actionOf(action));
		} catch (ActException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void handlePercept(String agName, Percept percept) {
		if (handledPers == 0) {
			Collection<Collection<Percept>> persEnts = null;
			try {
				persEnts = eisEnv.getAllPercepts(name).values();
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<Percept> percepts = new LinkedList<Percept>();
			for (Collection<Percept> persEnt : persEnts)
				percepts.addAll(persEnt);
			persOfStep = percepts.size();
			handlePercepts(percepts);
		} else if (handledPers == persOfStep)
			handledPers = -1;
		handledPers++;
	}

	private void handlePercepts(List<Percept> percepts) {
		List<Literal> literalPercepts = new LinkedList<Literal>();
		for (Percept percept : percepts)
			literalPercepts.add(literalOf(percept));
		notifyPercepts(literalPercepts);
	}

	private Literal literalOf(Percept percept) {
		Iterator<Parameter> paramsIt = percept.getParameters().iterator();
		String terms = percept.getParameters().isEmpty() ? "" : "(";
		while (paramsIt.hasNext()) {
			terms += sanitizeTerm(paramsIt.next().toString());
			terms += paramsIt.hasNext() ? "," : ")";
		}
		return Literal.parseLiteral(sanitizeTerm(percept.getName()) + terms);
	}

	private void notifyPercepts(List<Literal> percepts) {
		AgentListner marsAgentListner = marsAgentListners.get(name);
		if (marsAgentListner != null)
			marsAgentListner.notifyPercepts(percepts);
	}

	private String sanitizeTerm(String term) {
		if (term.isEmpty())
			return "emp";
		return term.substring(0, 1).toLowerCase() + term.substring(1);
	}

}
