package env;

public class DebugUtils {

	public static void printMethod(String method, String... args){
		String str = method + "(";
		for(int i = 0; i < args.length; i++)
			str += args[i] + (i < args.length - 1 ? ", " : "");
		str += ")";
		System.out.println(str);
	}
	
	public static void printMethodResult(String method, String result, String... args){
		String str = method + "(";
		for(int i = 0; i < args.length; i++)
			str += args[i] + (i < args.length - 1 ? ", " : "");
		str += ")";
		str += " returning " + result;
		System.out.println(str);
	}

	
}
