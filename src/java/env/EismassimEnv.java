package env;

// Environment code for project massim
import jason.asSyntax.Structure;
import jason.environment.Environment;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import agent.AgentListner;


import eis.EILoader;
import eis.EnvironmentInterfaceStandard;
import eis.exceptions.AgentException;
import eis.exceptions.ManagementException;
import eis.exceptions.NoEnvironmentException;
import eis.exceptions.PerceiveException;
import eis.exceptions.RelationException;

public class EismassimEnv extends Environment {

	private static final String CN = "massim.eismassim.EnvironmentInterface";
	private static final String EISMASSIMCONFIG = "eismassimconfig.xml";

	private static Map<String, AgentListner> marsAgentListners = new HashMap<String, AgentListner>();
	private static Map<String, String> marsAgentUsernames;

	public static String attachMarsAgentListner(String agName,
			AgentListner marsAgentListner) {
		marsAgentListners.put(agName, marsAgentListner);
		if (marsAgentUsernames == null)
			readMarsUsernames();
		return marsAgentUsernames.get(agName);
	}

	private static Collection<String[]> readEntities(String fileName)
			throws ParserConfigurationException, SAXException, IOException {
		Collection<String[]> result = new LinkedList<String[]>();
		File file = new File(fileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();
		NodeList entities = doc.getElementsByTagName("entity");
		for (int s = 0; s < entities.getLength(); s++) {
			Element entityElement = (Element) entities.item(s);
			String entity = entityElement.getAttribute("name");
			String agName = entityElement.getAttribute("agent");
			String userName = entityElement.getAttribute("username");
			String[] entityEntry = { entity, agName, userName };
			result.add(entityEntry);
		}
		return result;
	}

	private static void readMarsUsernames() {
		try {
			marsAgentUsernames = new HashMap<String, String>();
			Collection<String[]> entities = readEntities(EISMASSIMCONFIG);
			for (String[] entityEntry : entities) {
				marsAgentUsernames.put(entityEntry[1], entityEntry[2]);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Map<String, EismassimAgentListner> agents;

	private EnvironmentInterfaceStandard ei;

	@Override
	public boolean executeAction(String agName, Structure action) {
		return agents.get(agName).executeAction(action);
	}

	/** Called before the MAS execution with the args informed in .mas2j */
	@Override
	public void init(String[] args) {
		super.init(args);
		try {
			agents = new HashMap<String, EismassimAgentListner>();
			ei = EILoader.fromClassName(CN);
			registAgents(readEntities(EISMASSIMCONFIG));
			ei.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registAgents(Collection<String[]> entityEntries)
			throws AgentException, RelationException, PerceiveException,
			NoEnvironmentException, ManagementException,
			ParserConfigurationException, SAXException, IOException {
		for (String[] entityEntry : entityEntries) {
			String entity = entityEntry[0];
			String agName = entityEntry[1];
			agents.put(agName, new EismassimAgentListner(ei, agName, entity,
					marsAgentListners));
		}
	}

	/** Called before the end of MAS execution */
	@Override
	public void stop() {
		super.stop();
	}

}
