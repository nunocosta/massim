package mapModel;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import mapModel.graph.Graph;
import mapModel.state.ScoredMapState;

public class MapModel {

	private static final int NONE = -1;
	protected Graph graph;
	protected boolean[] known;
	protected int[] knownVeichles;
	protected ScoredMapState mapState;
	protected int size;
	protected int team;
	protected int teams;
	protected int veichles;
	private int[] vertexOwner;

	public MapModel(int size, int teams, int veichles, String team) {
		this.graph = new Graph(size);
		this.known = new boolean[size];
		this.knownVeichles = new int[teams];
		this.team = teamOf(team);
		this.teams = teams;
		this.size = size;
		this.veichles = veichles;
		this.vertexOwner = new int[size];
		mapState = new ScoredMapState(graph, teams, veichles, this.team);
	}

	public void addEdge(String vertex1, String vertex2) {
		int v1 = vertexOf(vertex1);
		int v2 = vertexOf(vertex2);
		graph.addEdge(v1, v2);
	}

	public void addEdge(String vertex1, String vertex2, double cost) {
		int v1 = vertexOf(vertex1);
		int v2 = vertexOf(vertex2);
		graph.addEdge(v1, v2, cost);
	}

	public double getDistance(String start, String goal, double edgesFactor,
			double costsFactor) {
		int v1 = vertexOf(start);
		int v2 = vertexOf(goal);
		return graph.getDistance(v1, v2, edgesFactor, costsFactor);
	}

	public List<String> getPath(String start, String goal, double edgesFactor,
			double costsFactor) {
		int v1 = vertexOf(start);
		int v2 = vertexOf(goal);
		List<String> shortestPath = new LinkedList<String>();
		List<Integer> path = graph.getPath(v1, v2, edgesFactor, costsFactor);
		if (path == null)
			return null;
		for (int v : path)
			shortestPath.add(vertexIdOf(v));
		return shortestPath;
	}

	public double getScore() {
		mapState.color();
		return mapState.getScore(team);
	}

	public int getSize() {
		return size;
	}

	public double getValue(String vertex) {
		Integer v = vertexOf(vertex);
		return graph.getValue(v);
	}

	public boolean hasAllTeamPositions(){
		return mapState.hasAllTeamPositions();
	}

	public void putVeichle(String veichle, String vertex, String team,
			boolean enabled) {
		int a = veichleOf(veichle);
		int v = vertexOf(vertex);
		int t = teamOf(team);
		knownVeichles[t]++;
		if (enabled)
			mapState.putVeichle(a, v, t);
	}

	public void reset() {
		Arrays.fill(knownVeichles, 0);
		Arrays.fill(vertexOwner, NONE);
		mapState.reset();
	}

	public void setVertex(String vertex, double value) {
		int v = vertexOf(vertex);
		graph.setValue(v, value);
		known[v] = true;
	}

	public int setVertex(String vertex, String team) {
		int v = vertexOf(vertex);
		int t = teamOf(team);
		vertexOwner[v] = t;
		return v;
	}

	private int teamOf(String veichleId) {
		char chr = veichleId.charAt(0);
		return ((int) chr) - ((int) 'a');
	}

	private int veichleOf(String veichleId) {
		String cleanedStr = veichleId.substring(1);
		return Integer.valueOf(cleanedStr) - 1;
	}
	
	private String vertexIdOf(int v) {
		return "v" + v;
	}

	private int vertexOf(String vertexId) {
		String cleanedStr = vertexId.substring(1);
		return Integer.valueOf(cleanedStr);
	}

	public String getPosToMove(String vertexId, String role){
		int v = vertexOf(vertexId);
		int [] neighb = graph.getNeighbours(v);
		if(role.equals("explorer"))
			return getPosToMoveExplorer(v);
		else{
			int nextPosaux = v;
			double nextPospoint=graph.getValue(v);
			for(Integer n: neighb)
				if(graph.getValue(n)>nextPospoint){
					nextPospoint = graph.getValue(n);
					nextPosaux = n;
				}
			if(nextPosaux==v){
				Random rand = new Random();
				int nextPos = neighb[rand.nextInt(graph.getGrade(v))];
				return vertexIdOf(nextPos);
			}
				
			return vertexIdOf(nextPosaux);
		}
	}
	
	private String getPosToMoveExplorer(int v){
		int [] neighb = graph.getNeighbours(v);
		for(Integer n: neighb)
			if(!isVertexProbed(vertexIdOf(n)))
				return vertexIdOf(n);
		Random rand = new Random();
		int nextPos = neighb[rand.nextInt(graph.getGrade(v))];
		return vertexIdOf(nextPos);
	}
	
		public boolean isVertexSurveyed(String vertexId){
		int v = vertexOf(vertexId);
		int [] neighb = graph.getNeighbours(v);
		for(Integer n: neighb)
			if(graph.getCost(v, n)!=Graph.UNKNOWN_COST)
				return true;
		return false;
	}
	
	public boolean isVertexProbed(String vertexId){
		int v = vertexOf(vertexId);
		return known[v];
	}
}
