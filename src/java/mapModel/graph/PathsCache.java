package mapModel.graph;


import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;



class PathsCache {
	private static final int CAPACYTY = 20;
	private Graph graph;
	private Map<String, Double> distances;
	private Map<String, List<Integer>> paths;
	private Queue<String> puts;

	PathsCache(Graph graph) {
		this.graph = graph;
		this.paths = new HashMap<String, List<Integer>>(CAPACYTY);
		this.distances = new HashMap<String, Double>(CAPACYTY);
		this.puts = new LinkedList<String>();
	}

	private void clean() {
		if (puts.size() > CAPACYTY) {
			String key = puts.poll();
			paths.remove(key);
			distances.remove(key);
		}
	}

	private void computePath(int i, int j, double edgesFactor,
			double costsFactor) {
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph, i,
				j, edgesFactor, costsFactor);
		dijkstra.execute();
		put(i, j, edgesFactor, costsFactor, dijkstra.getPath(),
				dijkstra.getDistance());
	}

	double getDistance(int i, int j, double edgesFactor, double costsFactor) {
		String key = key(i, j, edgesFactor, costsFactor);
		if (distances.get(key) == null)
			computePath(i, j, edgesFactor, costsFactor);
		putNextPath(key, edgesFactor, costsFactor);
		double distance = distances.get(key);
		clean();
		return distance;
	}

	List<Integer> getPath(int i, int j, double edgesFactor, double costsFactor) {
		String key = key(i, j, edgesFactor, costsFactor);
		if (paths.get(key) == null)
			computePath(i, j, edgesFactor, costsFactor);
		List<Integer> path = paths.get(key);
		if (i > j)
			path = reversed(path);
		putNextPath(path, distances.get(key), edgesFactor, costsFactor);
		clean();
		return path;
	}

	private String key(int i, int j, double edgesFactor, double costsFactor) {
		String key = edgesFactor + ";" + costsFactor + ":" + Math.min(i, j)
				+ ">" + Math.max(i, j);
		return key;
	}

	private void put(int i, int j, double edgesFactor, double costsFactor,
			List<Integer> path, double distance) {
		if (i > j)
			path = reversed(path);
		String key = key(i, j, edgesFactor, costsFactor);
		paths.put(key, path);
		distances.put(key, distance);
		puts.add(key);
	}

	private void putNextPath(List<Integer> path, double distance,
			double edgesFactor, double costsFactor) {
		if (path == null)
			return;
		if (path.size() <= 2)
			return;
		double c = edgesFactor + costsFactor * graph.getCost(path.get(0), path.get(1));
		List<Integer> nextPath = path.subList(1, path.size());
		double nextDistance = distance - c;
		put(path.get(1), path.get(path.size() - 1), edgesFactor, costsFactor,
				nextPath, nextDistance);
	}

	private void putNextPath(String key, double edgesFactor, double costsFactor) {
		putNextPath(paths.get(key), distances.get(key), edgesFactor,
				costsFactor);
	}

	private List<Integer> reversed(List<Integer> path) {
		if(path==null)
			return null;
		List<Integer> reversedPath = new LinkedList<Integer>(path);
		Collections.reverse(reversedPath);
		return reversedPath;
	}

}
