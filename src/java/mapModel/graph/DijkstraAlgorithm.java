package mapModel.graph;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class DijkstraAlgorithm {
	
	private static final int UNDEFINED = -1;
	Graph graph;
	int start;
	int goal;
	double[] dist;
	int[] previous;
	boolean[] visited;
	List<Integer> toExplore;
	double edgesFactor;
	double costsFactor;
	
	DijkstraAlgorithm(Graph graph, int startN, int goalN, double edgesFactor, double costsFactor){
		this.graph = graph;
		start = startN;
		goal = goalN;
		this.edgesFactor = edgesFactor;
		this.costsFactor = costsFactor;
		dist = new double[graph.getSize()];
		previous = new int[graph.getSize()];
		visited = new boolean[graph.getSize()];
		toExplore = new LinkedList<Integer>();
		initialize();
	}
	
	private void initialize(){
		for(int node=0; node<graph.getSize(); node++){
			dist[node] = Integer.MAX_VALUE;
			previous[node] = UNDEFINED;
			visited[node] = false;
		}
		toExplore.add(start);
		dist[start] = 0;
	}
	
	
	public void execute(){
		Integer v;
		while(!toExplore.isEmpty()){
			v = selectVertex(toExplore);
			if (v==goal) break;
			toExplore.remove(v);
			visited[v] = true;
			double aux;
			for(int n : graph.getNeighbours(v)){
				double c = edgesFactor + costsFactor*graph.getCost(v, n);
				aux = dist[v] + c;
				if (aux < dist[n]){
					dist[n] = aux;
					previous[n] = v;
					if(!visited[n])
						toExplore.add(n);
				}
			}
		}
	}
	
	private int selectVertex(List<Integer> e){
		Iterator<Integer> it = e.iterator();
		int v = it.next();
		while(it.hasNext()){
			int aux = it.next();
			if (dist[aux] < dist[v] && !visited[aux])
				v = aux;
		}
		return v;
	}
	
	public LinkedList<Integer> getPath(){
		if(dist[goal]==Integer.MAX_VALUE)
			return null;
		LinkedList<Integer> seq = new LinkedList<Integer>();
		int v = goal;
		while(previous[v]!=UNDEFINED){
			seq.addFirst(v);
			v = previous[v];
		}
		seq.addFirst(start);
		return seq;
	}
	
	public double getDistance(){
		return dist[goal];
	}
}
