package mapModel.graph;


import java.util.Arrays;
import java.util.List;



public class Graph {

	static final double NONE = -1;
	public static final int UNKNOWN_COST = Integer.MAX_VALUE;
	public static final int UNKNOWN_VALUE = 1;
	private int[][] adjacencies;
	private PathsCache cache;
	private double[][] costs;
	private int[] grades;
	private int size;
	private double[] values;

	public Graph(int size) {
		this.adjacencies = new int[size][size];
		this.costs = new double[size][size];
		this.grades = new int[size];
		this.size = size;
		this.values = new double[size];
		this.cache = new PathsCache(this);
		init();
	}

	
	public void addEdge(int vertex1, int vertex2) {
		addEdge(vertex1, vertex2, UNKNOWN_COST);
	}


	
	public void addEdge(int vertex1, int vertex2, double cost) {
		if(costs[vertex1][vertex2] == NONE){
			adjacencies[vertex1][grades[vertex1]++] = vertex2;
			adjacencies[vertex2][grades[vertex2]++] = vertex1;
		}
		costs[vertex1][vertex2] = costs[vertex2][vertex1] = cost;
	}

	public double getCost(int vertex1, int vertex2) {
		return costs[vertex1][vertex2];
	}

	public double getDistance(int start, int goal, double edgesFactor,
			double costsFactor) {
		return cache.getDistance(start, goal, edgesFactor, costsFactor);
	}

	public int getGrade(int vertex){
		return grades[vertex];
	}
	
	public int[] getNeighbours(int vertex) {
		return Arrays.copyOf(adjacencies[vertex], grades[vertex]);
	}

	public List<Integer> getPath(int start, int goal, double edgesFactor,
			double costsFactor) {
		return cache.getPath(start, goal, edgesFactor, costsFactor);
	}

	public int getSize() {
		return size;
	}

	public double getValue(int vertex) {
		return values[vertex];
	}

	private void init() {
		Arrays.fill(values, UNKNOWN_VALUE);
		for(double [] arr : costs)
			Arrays.fill(arr, NONE);
	}

	public void setValue(int vertex, double value) {
		values[vertex] = value;
	}
}
