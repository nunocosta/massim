package mapModel.state;

import java.util.Arrays;

import env.DebugUtils;

import mapModel.graph.Graph;


public class ScoredMapState extends MapState{
	
	private int [] c;
	private int [][] s;
	private int [] score;
	
	private class TeamParts{
		private int team;
		private int[] part;
		private boolean[] isolated;
		private int[] adjacentParts;
		private int[][] adjacentPartsList;
		private int[] partValue;
		
		private boolean hasOpponents(int vertex){
			return ag[vertex] - agT[vertex][team] == 0;
		}
		
		
			
		private TeamParts(int team) {
			int size = graph.getSize();
			this.team = team;
			this.part = new int[size];
			this.isolated = new boolean[size];
			this.adjacentParts = new int[size];
			this.adjacentPartsList = new int[size][size];
			this.partValue = new int[size];
			Arrays.fill(isolated, true);
		}



		private int searchPart(int[] frontier, int frontierStart, int frontierEnd,
				boolean[] visited) {
			int p = frontier[0];
			while(frontierStart < frontierEnd & isolated[p]){
				int v = frontier[frontierStart++];
				for(int vn : graph.getNeighbours(v)){
					if(c[vn] != team){
						if(hasOpponents(vn)){
							isolated[p] = false;
							break;
						}
						frontier[frontierEnd++] = vn;
					} 
					else
						adjacentPartsList[vn][adjacentParts[vn]++] = p;
					if(visited[vn])
						return 0;
				}
				part[v] = p;
				partValue[p] += graph.getValue(v);
				visited[v] = true;
			}
			return isolated[p] ? partValue[p] : 0;
		}

	}

	private TeamParts [] teamsParts;
	
	
	
	public ScoredMapState(Graph graph, int teams, int veichles, int team) {
		super(graph,  teams, veichles, team);
		int size = graph.getSize();
		c = new int [size];
		s = new int [size][teams];
		score = new int [teams];
		teamsParts = new TeamParts[teams];
		for(int t = 0; t < teams; t++)
			teamsParts[t] = new TeamParts(t);
	}
	
	public void color(){
		firstPhase();
	}
		
	private int majorityTeam(int v){
		for(int t = 0; t<teams; t++){
			if(agT[v][t] > (ag[v]/2))
				return t;
		}
		return NONE;
	}
	
	public double getScore(int team){
		
		
		DebugUtils.printMethodResult("getScore", ""+score[team], ""+team);
		
		return score[team];
	}
			
	private void firstPhase(){
		System.out.println("first pahse");

		
		int size = graph.getSize();
		int[] domin1 = new int [size];
		int domin1Size = 0;
		int[][] domin2 = new int [teams][size];
		int[] domin2Size = new int[teams];
		for(int i=0; i<occupiedSize; i++){
			int v = occupied[i];
			int t = majorityTeam(v);
			
			//prepare 2º phase
			if(t!=NONE){
				c[v] = t;
				score[t] += graph.getValue(v);
				domin1[domin1Size++] = v;
				
				if(t == 0){
					System.out.println("(" + v + ", " + ag[v] + ", " + agT[v][0] + ", " + agT[v][1] + ", " + t + ")" );
					System.out.println(score[0]);
				}

								
				for(int vn : graph.getNeighbours(v))
					if(ag[vn] == 0)
						s[vn][t]++;
				
				//prepare 3º phase
				domin2[t][domin2Size[t]++] = v;
			}
		}
		secondPhase(domin1, domin1Size, domin2, domin2Size);
	}
	
	private int neighbourhoodDominatorTeam(int v){
		int sMax = 1;
		int tMax = NONE;
		for(int t = 0; t < teams; t++)
			if(s[v][t] > sMax){
				sMax = s[v][t];
				tMax = t;
			}
		return tMax;
	}
	
	private void secondPhase(int [] domin1, int domin1Size, int [][] domin2, int [] domin2Size){
		System.out.println("second phase");
		boolean [] visited = new boolean[graph.getSize()];
		for(int i = 0; i < domin1Size; i++){
			int v = domin1[i];
			for(int vn : graph.getNeighbours(v)){
				if(!visited[vn]){
					int t = neighbourhoodDominatorTeam(vn);
					if(t != NONE){
						c[vn] = t;
						score[t] += graph.getValue(vn);
						domin1[domin1Size++] = vn;
						
						
						if(t == 0){
							System.out.println("(" + v + ", " + vn + ", " + graph.getGrade(v) + ", " + s[v][0] + ", " + s[v][1] + ", " + t + ")" );
							System.out.println(score[0]);
						}	
							
						//prepare 3ª phase
						domin2[t][domin2Size[t]] = vn;
					}
					visited[vn] = true;
				}
			}
		}
		//thirdPhase(domin2, domin2Size);
	}

	private void thirdPhase(int [][] domin2, int [] domin2Sizes){
		int size = graph.getSize();
		int [] frontier = new int [size];			
		for(int t = 0; t < teams; t++){
			boolean [] visited = new boolean [size];
			for(int d = 0; d < domin2Sizes[t]; d++){
				frontier[0] = domin2[t][d];
				score[t] = teamsParts[t].searchPart(frontier, 0, 1, visited);
			}
		}

	}
	
	
	
	
}