package mapModel.state;

import java.util.Arrays;

import env.DebugUtils;

import mapModel.graph.Graph;

public class MapState {

	protected static final int NONE = -1;

	protected int[] ag;
	protected int[][] agT;
	protected Graph graph;
	protected int[] occupied;
	protected int occupiedSize;
	protected int[][] positions;
	protected int team;
	protected int teams;
	protected int veichles;

	public MapState(Graph graph, int teams, int veichles, int team) {
		this.graph = graph;
		this.ag = new int[graph.getSize()];
		this.agT = new int[graph.getSize()][teams];
		this.occupied = new int[graph.getSize()];
		this.positions = new int[teams][veichles];
		this.teams = teams;
		this.veichles = veichles;
		for(int t = 0; t < teams; t++)
			Arrays.fill(positions[t], NONE);
	}
	
	public boolean hasAllTeamPositions(){
		for(int a = 0; a < veichles; a++)
			if (positions[team][a]==NONE){
				DebugUtils.printMethodResult("hasAllTeamPositions", ""+false);
				return false;
			}
		DebugUtils.printMethodResult("hasAllTeamPositions", ""+true);
		return true;
	}
	

	public void putVeichle(int veichle, int vertex, int team) {
		if (ag[vertex] == 0)
			occupied[occupiedSize++] = vertex;
		int oldPosition = positions[team][veichle];
		positions[team][veichle] = vertex;
		if (oldPosition != NONE) {
			ag[oldPosition]--;
			agT[oldPosition][team]--;
		}
		ag[vertex]++;
		agT[vertex][team]++;
	}

	public void reset() {		
		occupiedSize = 0;
	}

}
